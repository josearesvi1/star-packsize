package com.example.demo.client;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class StarWarsAPIImplTest {

  @Test
  public void testGetCharactersFirstCall(){
    StarWarsAPIImpl client = new StarWarsAPIImpl();
    assertNotNull( client.getCharacters(Optional.empty()) );
  }

  @Test
  public void testGetCharactersSecondCall(){
    StarWarsAPIImpl client = new StarWarsAPIImpl();
    assertNotNull( client.getCharacters(Optional.of("https://swapi.dev/api/people/?page=2")) );
  }

  @Test
  public void testAnimatedIntro(){
    StarWarsAPIImpl client = new StarWarsAPIImpl();
    assertNotNull( client.generateIntro() );
  }

}