package com.example.demo.service;

import com.example.demo.client.StarWarsAPI;
import com.example.demo.pojo.Character;
import com.example.demo.pojo.CharacterPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StarWarsCharactersServiceImplTest {

  @Mock
  private StarWarsAPI starWarsAPI;

  @InjectMocks
  private StarWarsCharactersServiceImpl starWarsCharactersService;

  @Test
  public void testGetAll(){
    when(starWarsAPI.getCharacters(Optional.empty())).thenReturn(getFirstCharacterPage());
    when(starWarsAPI.getCharacters(Optional.of(getFirstCharacterPage().getNext()))).thenReturn(getLastCharacterPage());
    List<Character> charaterList = starWarsCharactersService.getAll();
    Assertions.assertEquals(5, charaterList.size());
  }

  private CharacterPage getFirstCharacterPage(){
    CharacterPage page = new CharacterPage();
    page.setResults(Arrays.asList(new Character("Jose"), new Character("Toño"), new Character("Eric")));
    page.setNext("next");
    return page;
  }

  private CharacterPage getLastCharacterPage(){
    CharacterPage page = new CharacterPage();
    page.setResults(Arrays.asList(new Character("Kevin"), new Character("Daniel")));
    page.setNext(null);
    return page;
  }


}