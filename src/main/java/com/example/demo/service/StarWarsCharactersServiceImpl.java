package com.example.demo.service;

import com.example.demo.client.StarWarsAPI;
import com.example.demo.pojo.Character;
import com.example.demo.pojo.CharacterPage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StarWarsCharactersServiceImpl implements StarWarsCharactersService {

  private final StarWarsAPI starWarsAPI;

  public StarWarsCharactersServiceImpl(StarWarsAPI starWarsAPI) {
    this.starWarsAPI = starWarsAPI;
  }

  @Override
  public List<Character> getAll() {
    List<Character> allCharacters = new ArrayList<>();
    try {
      Optional<String> page = Optional.empty();
      while(true) {
        CharacterPage characterPage = starWarsAPI.getCharacters(page);
        if (!characterPage.getResults().isEmpty()) {
          allCharacters.addAll(characterPage.getResults());
          page = Optional.ofNullable(characterPage.getNext());
          if(page.isEmpty()){
            break;
          }
        } else {
          break;
        }
      }
      return allCharacters;
    }catch (Exception e ){
      e.printStackTrace();
      throw e;
    }
  }
}
