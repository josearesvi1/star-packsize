package com.example.demo;

import com.example.demo.client.StarWarsAPIImpl;
import com.example.demo.pojo.Character;
import com.example.demo.service.StarWarsCharactersService;
import com.example.demo.service.StarWarsCharactersServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		final StarWarsCharactersServiceImpl service = new StarWarsCharactersServiceImpl(new StarWarsAPIImpl());
		System.out.println(service.getAll());
		System.out.println("Follow the link now: https://starwarsintrocreator.kassellabs.io/#!/ENTMBbVtHD2rkQb10eBo");
	}

}
