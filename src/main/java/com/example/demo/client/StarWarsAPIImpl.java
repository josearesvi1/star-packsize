package com.example.demo.client;


import com.example.demo.pojo.CharacterPage;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class StarWarsAPIImpl implements StarWarsAPI {

  private final RestTemplate restTemplate= new RestTemplate();
  private final String charactersStarWarsAPI = "https://swapi.dev/api/people";

  private HttpEntity request;

  public StarWarsAPIImpl() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    request = new HttpEntity(headers);
  }

  @Override
  public CharacterPage getCharacters(Optional<String> page) {
    ResponseEntity<CharacterPage> response;
    response = page.map(nextPageURL -> restTemplate.exchange(nextPageURL, HttpMethod.GET, request, CharacterPage.class))
        .orElseGet(() -> restTemplate.exchange(charactersStarWarsAPI, HttpMethod.GET, request, CharacterPage.class));
    return response.getBody();
  }

}
