package com.example.demo.client;

import com.example.demo.pojo.CharacterPage;

import java.util.Optional;

public interface StarWarsAPI {

  CharacterPage getCharacters(Optional<String> page);

}
