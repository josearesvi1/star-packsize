package com.example.demo.pojo;

import java.io.Serializable;

public class Character implements Serializable {

  private String name;

  public Character() {
  }

  public Character(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}
