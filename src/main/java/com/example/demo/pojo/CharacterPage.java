package com.example.demo.pojo;

import java.io.Serializable;
import java.util.List;

public class CharacterPage implements Serializable {

  private String next =  null;
  private List<Character> results;

  public CharacterPage() {
  }

  public CharacterPage(String next, List<Character> results) {
    this.next = next;
    this.results = results;
  }

  public String getNext() {
    return next;
  }

  public boolean hasNext() {
    return next != null;
  }


  public void setNext(String next) {
    this.next = next;
  }

  public List<Character> getResults() {
    return results;
  }

  public void setResults(List<Character> results) {
    this.results = results;
  }
}
